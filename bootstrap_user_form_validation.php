<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap 4 From validation</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

  <!-- bootstrap valid js link -->
  <script src="https://cdn.rawgit.com/PascaleBeier/bootstrap-validate/v2.2.0/dist/bootstrap-validate.js" ></script>

</head>
<body>

<div class="container mt-2">
  <form class="myfrom">
  	<h3 style="text-align: center; font-weight: bold; font-size: 25px;">Bootstrap Form Validation</h3>
  	<div class="row ">
  		<div class="col-md-6 offset-md-3 jumbotron">

  			<div class="card card-body">
  					<div class="form-group">
  						<label for="inputName">Full Name</label>
  						<input  type="text" id="fname" name="" class="form-control" placeholder="Name">
  					</div>
  					<div class="form-group">
  						<label for="inputName">Email Address</label>
  						<input  type="Email" id="email" name="" class="form-control" placeholder="Email Address">
  					</div>
  					<div class="form-group">
  						<label for="inputName">Password</label>
  						<input  type="text" id="pass" name="" class="form-control" placeholder="Password">
  					</div>
  					<div class="form-group">
  						<label for="inputName">Confirm Password</label>
  						<input  type="text" id="cpass" name="" class="form-control" placeholder="Confrom Password">
  					</div>
  					
  					<div class="form-group">
  						<label for="inputName">URL Address</label>
  						<input  type="text" id="url" name="" class="form-control" placeholder="URL Address">
  					</div>
  					<div class="form-group">
  						<label for="inputName">file</label>
  						<input  type="text" id="file" name="" class="form-control" placeholder="File">
  					</div>

  				
  				
  			</div>
  			
  		</div>

  	</div>	
  	
  </form>


</div>

</body>

<script>
	bootstrapValidate('#fname','min:5:Enter at least 6 characters')
	bootstrapValidate('#email','email:Enter a valid email')
	bootstrapValidate('#cpass','matches:#pass:Your Password Should Match!')
	bootstrapValidate('#url','url:Enter a valid URL !')
	bootstrapValidate('#file','required: File Moust Be  required!')
</script>
</html>

